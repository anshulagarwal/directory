package me.anshulagarwal.directory;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;

import me.anshulagarwal.directory.Base.BaseActivity;
import me.anshulagarwal.directory.fragments.DirectoryFragment;

public class MainActivity extends BaseActivity {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        if (savedInstanceState == null) {
            addFragment(DirectoryFragment.getInstance(getApp().getRootDirector()), false, DirectoryFragment.TAG);
        }
    }

    @Override
    protected void configureToolbar() {

    }

    @Override
    protected void setUpNavigationView() {

    }

    @Override
    public int getFrameLayoutId() {
        return R.id.content;
    }

    @Override
    public String getName() {
        return "Directory";
    }
}
