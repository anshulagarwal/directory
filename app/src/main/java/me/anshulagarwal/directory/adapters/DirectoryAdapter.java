package me.anshulagarwal.directory.adapters;

import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.ArrayList;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anshulagarwal.directory.Models.Directory;
import me.anshulagarwal.directory.R;

/**
 * Created by Shine on 30/04/17.
 */

public class DirectoryAdapter extends RecyclerView.Adapter<DirectoryAdapter.DirectoryViewHolder> {


    private final ArrayList<Directory> mDirectoryList;
    private final ItemClickListener mListener;

    public DirectoryAdapter(ArrayList<Directory> directories, ItemClickListener listener) {

        mDirectoryList = directories;
        mListener = listener;
    }

    @Override
    public DirectoryViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View item = LayoutInflater.from(parent.getContext()).inflate(R.layout.directory_list_item, parent, false);


        return new DirectoryViewHolder(item);
    }

    @Override
    public void onBindViewHolder(DirectoryViewHolder holder, int position) {

        holder.bindData(mDirectoryList.get(position));
    }

    @Override
    public int getItemCount() {
        return mDirectoryList == null ? 0 : mDirectoryList.size();
    }

    public class DirectoryViewHolder extends RecyclerView.ViewHolder {

        @BindView(R.id.name)
        TextView mName;

        @BindView(R.id.child_count)
        TextView mChildCount;

        public DirectoryViewHolder(View itemView) {
            super(itemView);
            ButterKnife.bind(this, itemView);
        }

        public void bindData(final Directory directory) {

            mName.setText(directory.mName);

            if (directory.getChilds().size() != 0) {
                mChildCount.setText(directory.getChilds().size() + " items");
                mChildCount.setVisibility(View.VISIBLE);
            } else {
                mChildCount.setVisibility(View.GONE);
            }
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    mListener.onItemClick(directory);
                }
            });

        }
    }

    public interface ItemClickListener {

        public void onItemClick(Directory directory);
    }
}
