package me.anshulagarwal.directory;

import android.app.Application;

import me.anshulagarwal.directory.Models.Directory;

/**
 * Created by Shine on 30/04/17.
 */

public class App extends Application {

    private Directory mRoot;

    @Override
    public void onCreate() {
        super.onCreate();
        createRootDirectory();
    }

    private void createRootDirectory() {
        mRoot = new Directory("Root");

    }

    public Directory getRootDirector() {
        return mRoot;
    }
}
