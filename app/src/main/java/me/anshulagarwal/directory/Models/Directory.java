package me.anshulagarwal.directory.Models;

import android.text.TextUtils;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

/**
 * Created by Shine on 30/04/17.
 */

public class Directory implements Serializable {

    public String mName;

    public Directory mParent;

    public ArrayList<Directory> mChilds;

    private Calendar mCreatedDate;

    public Directory(String name) {
        if (TextUtils.isEmpty(name)) {
            new RuntimeException("Directory name cannot be empty");
        }
        mName = name;
        mCreatedDate = Calendar.getInstance();
        mChilds = new ArrayList<>();
    }

    public Directory(String name, Directory parent) {

        this(name);
        if (parent == null) {
            new RuntimeException("Directory Parent cannot be empty");
        }
        mParent = parent;
    }

    public void addChild(Directory directory) {
        mChilds.add(directory);
    }

    public ArrayList<Directory> getChilds() {
        return mChilds;
    }
}
