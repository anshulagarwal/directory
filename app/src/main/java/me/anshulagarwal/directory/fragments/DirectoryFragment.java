package me.anshulagarwal.directory.fragments;

import android.content.DialogInterface;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.app.AlertDialog;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.TextUtils;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import java.util.Stack;

import butterknife.BindView;
import butterknife.ButterKnife;
import me.anshulagarwal.directory.Base.BaseFragment;
import me.anshulagarwal.directory.Models.Directory;
import me.anshulagarwal.directory.R;
import me.anshulagarwal.directory.adapters.DirectoryAdapter;

/**
 * Created by Shine on 30/04/17.
 */

public class DirectoryFragment extends BaseFragment implements DirectoryAdapter.ItemClickListener {

    public static String TAG = DirectoryFragment.class.getSimpleName();
    private static String BUNDLE_DIRECTORY = "bundle_directory";
    private DirectoryAdapter mAdapter;


    public static DirectoryFragment getInstance(Directory directory) {

        DirectoryFragment fragment = new DirectoryFragment();
        Bundle bundle = new Bundle();
        bundle.putSerializable(BUNDLE_DIRECTORY, directory);
        fragment.setArguments(bundle);
        return fragment;

    }

    private Directory mDirectory;

    @BindView(R.id.recyclerView)
    RecyclerView mRecyclerView;

    @BindView(R.id.empty_directory)
    TextView mEmptyView;

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        Bundle arg = getArguments();

        mDirectory = (Directory) arg.getSerializable(BUNDLE_DIRECTORY);
        setHasOptionsMenu(true);

    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_directory, container, false);
        ButterKnife.bind(this, view);
        return view;
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (mDirectory.getChilds().size() != 0) {
            mRecyclerView.setVisibility(View.VISIBLE);
            mEmptyView.setVisibility(View.GONE);

        }
        mAdapter = new DirectoryAdapter(mDirectory.getChilds(), this);

        mRecyclerView.setLayoutManager(new LinearLayoutManager(getContext()));
        mRecyclerView.setAdapter(mAdapter);

    }

    @Override
    public void onCreateOptionsMenu(Menu menu, MenuInflater inflater) {
        super.onCreateOptionsMenu(menu, inflater);

        inflater.inflate(R.menu.directory_menu, menu);
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        if (item.getItemId() == R.id.add_directory) {
            addChildDirectory();
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    private void addChildDirectory() {
        AlertDialog.Builder dialogBuilder = new AlertDialog.Builder(getContext());
        LayoutInflater inflater = getActivity().getLayoutInflater();
        final View dialogView = inflater.inflate(R.layout.custom_dialog, null);
        dialogBuilder.setView(dialogView);

        final EditText dirName = (EditText) dialogView.findViewById(R.id.directory_name);

        dialogBuilder.setTitle(getString(R.string.create_directory));
        //dialogBuilder.setMessage("Enter text below");
        dialogBuilder.setPositiveButton(getString(R.string.create), new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //do something with edt.getText().toString();
                if (TextUtils.isEmpty(dirName.getText())) {
                    Toast.makeText(getContext(), "Please enter directory name", Toast.LENGTH_SHORT).show();
                    return;
                }

                mDirectory.addChild(new Directory(dirName.getText().toString(), mDirectory));
                hideEmptyView();

            }
        });

        dialogBuilder.setNegativeButton("Cancel", new DialogInterface.OnClickListener() {
            public void onClick(DialogInterface dialog, int whichButton) {
                //pass
            }
        });

        AlertDialog dialog = dialogBuilder.create();
        dialog.show();
    }

    private void hideEmptyView() {
        mEmptyView.setVisibility(View.GONE);
        mRecyclerView.setVisibility(View.VISIBLE);
    }

    @Override
    public void onItemClick(Directory directory) {
        getFragmentListener().replaceFragment(DirectoryFragment.getInstance(directory), true, TAG);
    }

    @Override
    protected String screenTitle() {
        return mDirectory.mName;
    }
}
