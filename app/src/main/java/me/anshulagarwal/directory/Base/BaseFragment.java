package me.anshulagarwal.directory.Base;

import android.support.annotation.StringRes;

import com.nuvoex.library.LumiereBaseFragment;

/**
 * Created by Shine on 22/08/16.
 */
public abstract class BaseFragment extends LumiereBaseFragment {


    @Override
    public void onResume() {
        super.onResume();
    }

    public void popBackStackImmediate() {
        if (!getActivity().getSupportFragmentManager().isDestroyed()) {
            getActivity().getSupportFragmentManager().popBackStackImmediate();
        }
    }

    public String getName() {
        return getClass().getCanonicalName();
    }




    public String formatString(@StringRes int id, Object... formatArgs) {
        return getString(id, formatArgs);
    }
}
