package me.anshulagarwal.directory.Base;

import android.os.Bundle;
import android.view.MenuItem;

import com.nuvoex.library.LumiereBaseActivity;

import butterknife.ButterKnife;
import me.anshulagarwal.directory.App;

/**
 * Created by Shine on 22/08/16.
 */
public abstract class BaseActivity extends LumiereBaseActivity implements com.nuvoex.library.FragmentInterface {

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
    }

    @Override
    public void setContentView(int layoutResId) {
        super.setContentView(layoutResId);
        ButterKnife.bind(this);
    }

    @Override
    public boolean onNavigationItemSelected(MenuItem item) {
        return false;
    }

    @Override
    protected boolean useNavDrawer() {
        return false;
    }


    public App getApp() {
        return (App) getApplication();
    }

}
